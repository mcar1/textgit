﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


int F7(char c[])
{
	int i, s;
	for (i = 0; c[i] != '\0'; i++)
		if (c[i] >= '0' && c[i] <= '7') break;
	for (s = 0; c[i] >= '0' && c[i] <= '7'; i++)
		s = s * 8 + c[i] - '0';
	return s;
}

int main()
{
	char a[100];
	char b[100]="";
	gets_s(a);
	int i=0,s,j=0;
	bool c = true; //есть ли дальше число?
	while (c)
	{
		c = false;
		for (; a[i] != '\0'; i++, j++)
			if (a[i] >= '0' && a[i] <= '7') { c = true; break;}
			else b[j] = a[i];
		for (s = 0; a[i] >= '0' && a[i] <= '7'; i++)
			s = s * 8 + a[i] - '0';
		if(c) b[j++] = s;
	}
	puts(b);
}